package com.example.restdemo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Post {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne(optional=false)
	private User user;
	
	private String content;

	public Post() {
	}
	public Post(User u, String c) {
		user = u;
		content = c;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String c) {
		content = c;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User u) {
		user = u;
	}
}
